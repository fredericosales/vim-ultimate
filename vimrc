"
"" vimrc
"
" Frederico Sales
" <frederico.sales@engenharia.ufjf.br>
" 2018
"

" primal stuff
syntax on
set nocompatible
set encoding=UTF-8
set pyxversion=3
set nu relativenumber
set confirm
set hlsearch
set smartindent
set hls is
set ruler
set autowrite
set noswapfile
set ic
set incsearch
set showmatch
set showcmd
set clipboard+=unnamedplus
set background=dark
set shiftwidth=4
set tabstop=4
set expandtab
set softtabstop=4
set mouse=a
set timeoutlen=500
set tw=80
set fo+=t
set list lcs=tab:·⁖,trail:¶

"
"" filetype
"
filetype plugin on
filetype plugin indent on

"
"" autocmd
"
autocmd BufWritePre * :%s/\s\+$//e
autocmd BufWritePost * if &diff | diffupdate | endif

"
"" python3
"
let g:python3_host_prog="/usr/bin/python3"
nnoremap <F5> <esc> :w<cr> :echo system('/usr/bin/python3' "' .expand('%') . '"')<cr>

"
"" spell check
"
map <F8> :setlocal spell spelllang=en_us<cr>
map <F9> :setlocal spell spelllang=pt_br<cr>

"
"" better undo
"
if has('persistent_undo')          " check if your vim version supports
   set undodir=$HOME/.vim/undo     " directory where the undo files will be stored
   set undofile                    " turn on the feature
endif

"
"" select all
"
map <c-a> ggvG$
imap <c-a> ggcG$

"
"" functions
"

" remove trailling
"function  RemTrailling()
"    g:%s/\s\+$//e
"endfunction

" create a new directory to a new fresh file
augroup vimrc-auto-mkdir
    autocmd!
    autocmd BufWritePre * call s:auto_mkdir(expand('<afile>:p:h'), v:cmdbang)
    function! s:auto_mkdir(dir, force)
        if !isdirectory(a:dir)
            \   && (a:force
            \       || input("'" . a:dir . "' does not exist. Create? [y/N]") =~? '^y\%[es]$')
        call mkdir(iconv(a:dir, &encoding, &termencoding), 'p')
        endif
    endfunction
augroup END

"
"" vim-plug
"
call plug#begin('~/.config/vim/plugged')

" vimtex plugin
Plug 'lervag/vimtex'

" colorschemes
Plug 'rafi/awesome-vim-colorschemes'

" vimproc
Plug 'Shougo/vimproc.vim', { 'do': 'make' }

" css colors
Plug 'ap/vim-css-color'
Plug 'lilydjwg/colorizer'

" devicons
Plug 'ryanoasis/vim-devicons'

" vim-powerline
Plug 'powerline/powerline'

"vim-arline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" csv files
Plug 'chrisbra/csv.vim'

" pandoc
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'

" vim-wiki
Plug 'vimwiki/vimwiki'

" vim-gitgutter
Plug 'tpope/vim-fugitive'

" nerd commenter
Plug 'preservim/nerdcommenter'

" move A-k; A-j
Plug 'matze/vim-move'

" vim-go
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" deoplete
if has('nvim')
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
    Plug 'Shougo/deoplete.nvim'
    Plug 'roxma/nvim-yarp'
    Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

" tabmine
Plug 'tbodt/deoplete-tabnine', { 'do': './install.sh' }

call plug#end()
